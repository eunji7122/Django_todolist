from django.db import models
from datetime import datetime

# Create your models here.


class ToDoList(models.Model):
    title = models.CharField('TITLE', max_length=50)
    content = models.TextField('CONTENT', max_length=100, blank=True, help_text='simple content text.')
    start_date = models.DateTimeField('Start Date')
    end_date = models.DateTimeField('End Date')

    def __str__(self):
        return self.title
